const express = require("express");
const cors = require("cors");
const bodyParser = require('body-parser');
const createError = require('create-error');
const path = require('path');
const morgan = require("morgan");
const app = express();

const mongoose = require('mongoose');
const dbConfig = require('./database/db');

// Express Routes
const studentRoute = require('./routes/review.route');

const notificationRoute = require('./routes/notification.route');

// Connecting mongoDB Database
mongoose.Promise = global.Promise;

mongoose.connect(dbConfig.db, {
    useNewUrlParser: true,
    useUnifiedTopology: true
}).then(() => {
        console.log('Database successfully connected!')
    },
    error => {
        console.log('Could not connect to database : ' + error)
    }
)

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(cors());

// Using routes
app.use('/reviews', studentRoute);
app.use('/notifications', notificationRoute);

// Port
const port = process.env.PORT || 4000;
const server = app.listen(port, () => {
    console.log('Connected to port ' + port)
})

// Load the build
app.use(express.static(path.join(__dirname, 'build')));

app.get('/*', (req, res) => {
    res.sendFile(path.join(__dirname, 'build', 'index.html'));
});

// Morgan
app.use(morgan('combined'));

// 404 Error
app.use((req, res, next) => {
    next(createError(404));
});

// Error message
app.use(function (err, req, res, next) {
    console.error(err.message);
    if (!err.statusCode) err.statusCode = 500;
    res.status(err.statusCode).send(err.message);
});

