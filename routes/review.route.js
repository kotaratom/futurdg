const mongoose = require('mongoose'),
  express = require('express'),
  router = express.Router();

mongoose.set('useFindAndModify', false);

// Review Model
let reviewschema = require('../models/review');

// Create Review
router.route('/create-review').post((req, res, next) => {
  reviewschema.create(req.body, (error, data) => {
    if (error) {
      return next(error)
    } else {
      console.log(data)
      res.json(data)
    }
  })
});

// Read reviews
router.route('/').get((req, res) => {
  reviewschema.find((error, data) => {
    if (error) {
      return next(error)
    } else {
      res.json(data)
    }
  })
})

// Delete Review
router.route('/delete-review/:id').delete((req, res, next) => {
  reviewschema.findByIdAndRemove(req.params.id, (error, data) => {
    if (error) {
      return next(error);
    } else {
      res.status(200).json({
        msg: data
      })
    }
  })
})

module.exports = router;