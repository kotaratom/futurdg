const express = require('express');
const router = express.Router();
const moment = require('moment');
const sgMail = require('@sendgrid/mail')

// Load configuration from .env file
require('dotenv').config({ path: __dirname + `/../.env` });

// Load and initialize MessageBird SDK
const messagebird = require('messagebird')(process.env.MESSAGEBIRD_API_KEY);

function setDelay(preparation) {

    // Delay time with test value
    let time = 360;

    // Preparation data
    const preparationData = {
        one: ["1 den nejezte", "1 den neužívejte léky","1 den neužívat tabákové výrobky, neužívat návykové látky","Vzít s sebou výsledky předchozích vyšetření"],
        twelve_h: ["12 hodin nepijte, nejezte, neužívejte tabákové výrobky"],
        four_h: ["4 hodiny nejezte"],
        two_h: ["2 hodiny nepijte"],
    }

    // Compare
    for (let key in preparationData.one) {
        if (preparationData.one[key].localeCompare(preparation) === 0) {
            time = 86400;
        }
    }

    // Compare
    for (let key in preparationData.twelve_h) {
        if (preparationData.twelve_h[key].localeCompare(preparation) === 0) {
            time = 86400 / 2;
        }
    }

    // Compare
    for (let key in preparationData.four_h) {
        if (preparationData.four_h[key].localeCompare(preparation) === 0) {
            time = 86400 / 6;
        }
    }

    // Compare
    for (let key in preparationData.two_h) {
        if (preparationData.two_h[key].localeCompare(preparation) === 0) {
            time = 86400 / 12;
        }
    }

    console.log("final preparation", preparation);
    console.log("final delay", time);

    return time;
}

router.route('/create-notification').post((req, res) => {

    const name = req.body.name;
    const screening = req.body.screening;
    const treatment = req.body.preparation;
    const email = req.body.email;
    const phone = req.body.phone;
    const date = req.body.date;
    const time = req.body.time;

    // Date of the appointment
    const appointmentDt = moment(req.body.date + " " + req.body.time);

    console.log("prep2 is:", req.body.preparation);

    // Date of the reminder
    const reminderDt = appointmentDt.clone().subtract(setDelay(req.body.preparation), "seconds");

    // Conversion to UNIX datetime
    const reminderDtUnix = moment(reminderDt).unix();

    // SendGrid API
    sgMail.setApiKey(process.env.SENDGRID_API_KEY)
    const msg = {
        to: email,
        from: {
            email: 'notifikace@futurdg.com',
            name: 'futuRDG'
        },
        subject: 'Upozornění na vyšetření v nemocnici',
        html: `<div>
                   <p>Jméno: ${name}</p>
                   <p>Vyšetření: ${screening}</p>
                   <p>Příprava: ${treatment}</p>
                   <p>Email: ${email}</p>
                   <p>Telefon: ${phone}</p>
                   <p>Datum vyšetření: ${moment(date).locale("cs").format('LL')}</p>
                   <p>Čas vyšetření: ${time}</p>
                   <p>Brzy se uvidíme!</p>
               </div>`,
        send_at: reminderDtUnix,
    }
    sgMail
        .send(msg)
        .then(() => {
            console.log('Email sent')
        })
        .catch((error) => {
            console.error(error);
            if (error) {
                return next(error);
            } else {
                res.status(200).json({
                    msg: data
                })
            }
        })

    // Message bird
    messagebird.lookup.read(req.body.phone, process.env.COUNTRY_CODE, function (err, response) {
        if (err) {
            return console.log(err);
        }

        // Send scheduled message with MessageBird API
        messagebird.messages.create({
            originator: "futuRDG",
            recipients: [response.phoneNumber], // normalized phone number from lookup request
            scheduledDatetime: reminderDt.format(),

            // Full message
            body: name + ", zde je upozornění na přípravu pro vyšetření " + screening + " naplánované na " + appointmentDt.locale("cs").format('LLLL') + ". " + treatment + ". Brzy se uvidíme!"

        }, function (err, response) {
            if (err) {
                // Request has failed
                console.log(err);
                console.log("Error occured while sending sms!");
                res.json({status: "error"});
            } else {
                // Request was successful
                console.log(response);

                // Console log successful
                console.log("SMS sent successfully");
                res.json({status: "sent"});
            }
        });

        console.log(response);
    });

});

module.exports = router;