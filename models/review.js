const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let reviewschema = new Schema({
  title: {
    type: String
  },
  rating: {
    type: Number
  },
  content: {
    type: String
  }
}, {
    collection: 'reviews'
  })

module.exports = mongoose.model('Review', reviewschema)